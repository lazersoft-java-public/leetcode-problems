# leetcode problems



## Getting started

This is a basic Java 11 project, with solutions to various programming problems from https://leetcode.com/

Mostly, problems were selected from medium and hard, focusing on most accepted ones:

https://leetcode.com/problemset/all/?page=1&sorting=W3sic29ydE9yZGVyIjoiREVTQ0VORElORyIsIm9yZGVyQnkiOiJBQ19SQVRFIn1d&difficulty=MEDIUM