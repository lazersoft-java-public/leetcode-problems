package p1;

// https://leetcode.com/problems/minimum-domino-rotations-for-equal-row/
public class Minimum_domino_rotations_for_equal_row {

	public int minDominoRotations(int[] tops, int[] bottoms) {
		
		var rotations = new int[2];
		var top = tops[0];
		var bottom = bottoms[0];
		var unrotatableCount = 0;
		
        for(var i = 1; i < tops.length; i++) {
        	
        	var curTop = tops[i];
        	var curBottom = bottoms[i];
        	
        	if(curTop == curBottom) {
        		unrotatableCount++;
        	}
        	
        	if(rotations[0] != -1 && top != curTop) {
        		if(top == curBottom) {
        			rotations[0] = rotations[0] + 1;
        		} else {
        			rotations[0] = -1;
        		}
        	}
        	
        	if(rotations[1] != -1 && bottom != curBottom) {
        		if(bottom == curTop) {
        			rotations[1] = rotations[1] + 1;
        		} else {
        			rotations[1] = -1;
        		}
        	}
        	
        	if(rotations[0] == -1 && rotations[1] == -1) {
        		return -1;
        	}
        }
        
        if(rotations[0] == 0 || rotations[1] == 0) {
        	return 0;
        }
        
        var minRotationsTop = rotations[0] == -1 ? -1 : getActualMinRotations(rotations[0], tops.length, unrotatableCount);
        var minRotationsBottom = rotations[1] == -1 ? -1 : getActualMinRotations(rotations[1], tops.length, unrotatableCount);
        if(minRotationsTop == -1) {
        	return minRotationsBottom;
        }
        if(minRotationsBottom == -1) {
        	return minRotationsTop;
        }
        return Math.min(minRotationsTop, minRotationsBottom);
    }
	
	private int getActualMinRotations(int minRotations, int inputLength, int unrotatableCount) {
		if(minRotations >= (Math.floor(inputLength / 2) + 1 - unrotatableCount)) {
			return Math.min(minRotations, inputLength - minRotations - unrotatableCount);
		} else {
			return minRotations;
		}
	}
}
