package p1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// https://leetcode.com/problems/letter-combinations-of-a-phone-number/
public class Letter_combinations_of_a_phone_number {
	
	private static final short CHAR_DIFF = 97;

	public List<String> letterCombinations(String digits) {
		if(digits.length() == 0) {
			return new ArrayList<>();
		}
		
		short[] digitsArr = parse(digits);
		
		List<String> combinations = new ArrayList<>();
		var phoneMap = initPhoneMap();
		
		var maxCharsIdx = getMaxCharsIdx(digitsArr, phoneMap);
		
		var charsIdx = new short[digits.length()];
		while(!Arrays.equals(maxCharsIdx, charsIdx)) {
			combinations.add(getCombination(charsIdx, digitsArr, phoneMap));
			for(var i = charsIdx.length - 1; i >= 0; i--) {
				var nextVal = charsIdx[i] + 1;
				if(nextVal <= maxCharsIdx[i]) {
					charsIdx[i] = (short) nextVal;
					break;
				} else {
					charsIdx[i] = 0;
				}
			}
		}
		combinations.add(getCombination(charsIdx, digitsArr, phoneMap));
		
		return combinations;
    }
	
	private short[] getMaxCharsIdx(short[] digitsArr, short[][] phoneMap) {
		var maxCharsIdx = new short[digitsArr.length];
		for(var i = 0; i < digitsArr.length; i++) {
			var digit = digitsArr[i];
			var maxCharsForDigit = phoneMap[digit].length - 1;
			maxCharsIdx[i] = (short) maxCharsForDigit;
		}
		return maxCharsIdx;
	}
	
	private String getCombination(short[] charsIdx, short[] digitsArr, short[][] phoneMap) {
		var combinationChars = new char[charsIdx.length];
		for(short i = 0; i < charsIdx.length; i++) {
			short digit = digitsArr[i];
			var zbChars = phoneMap[digit];
			combinationChars[i] = (char) (zbChars[charsIdx[i]] + CHAR_DIFF);
		}
		return new String(combinationChars);
	}
	
	private short[][] initPhoneMap() {
		short[][] map = new short[12][];
		
		map[2] = new short[] { 'a'- CHAR_DIFF, 'b' - CHAR_DIFF, 'c' - CHAR_DIFF };
		map[3] = new short[] { 'd' - CHAR_DIFF, 'e' - CHAR_DIFF, 'f' - CHAR_DIFF };
		map[4] = new short[] { 'g' - CHAR_DIFF, 'h' - CHAR_DIFF, 'i' - CHAR_DIFF };
		map[5] = new short[] { 'j' - CHAR_DIFF, 'k' - CHAR_DIFF, 'l' - CHAR_DIFF };
		map[6] = new short[] { 'm' - CHAR_DIFF, 'n' - CHAR_DIFF, 'o' - CHAR_DIFF };
		map[7] = new short[] { 'p' - CHAR_DIFF, 'q' - CHAR_DIFF, 'r' - CHAR_DIFF, 's' - CHAR_DIFF };
		map[8] = new short[] { 't' - CHAR_DIFF, 'u' - CHAR_DIFF, 'v' - CHAR_DIFF };
		map[9] = new short[] { 'w' - CHAR_DIFF, 'x' - CHAR_DIFF, 'y' - CHAR_DIFF, 'z' - CHAR_DIFF };
		return map;
	}
	
	private short[] parse(String digits) {
		var a = new short[digits.length()];
		for(int i = 0; i < digits.length(); i++) {
			a[i] = Short.parseShort(digits.substring(i, i + 1));
		}
		return a;
	}
	
	public static void main(String[] args) {
//		System.out.println("a=" + (int)'a');
//		System.out.println("b=" + (int)'b');
//		System.out.println("c=" + (int)'c');
//		System.out.println("...");
//		System.out.println("x=" + (int)'x');
//		System.out.println("y=" + (int)'y');
//		System.out.println("z=" + (int)'z');
//		System.out.println("#=" + (int)'#');
//		System.out.println("[space]=" + (int)' ');
//		System.out.println("*=" + (int)'*');
//		
//		System.out.println(new String(new char[] {99, 43 , 43}));
//		
//		char[] c = new String("a b+c#d").toCharArray();
//		Arrays.sort(c);
//		System.out.println(new String(c));
		
		Letter_combinations_of_a_phone_number a = new Letter_combinations_of_a_phone_number();
		a.letterCombinations("23");
	}
}
