package p1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// https://leetcode.com/problems/find-all-anagrams-in-a-string/
public class Find_all_anagrams_in_a_string {
    public List<Integer> findAnagrams(String s, String p) {
		if(!validate(s, p)) {
			return new ArrayList<>();
		}
        
        if(s.length() == p.length() && s.equals(p)) {
            return Arrays.asList(1);
        }
        
		var occurences = new ArrayList<Integer>();
		var pCharsCount = new CharCountMap(p);
		var slidingWindowCharsCount = new CharCountMap(s.substring(0, p.length()));
		if(slidingWindowCharsCount.equals(pCharsCount)) {
			occurences.add(0);
		}
		for(var i = 1; i < s.length() - p.length() + 1; i++) {
			var cToRemove = s.charAt(i - 1);
			var cToAdd = s.charAt(i + p.length() - 1);
			slidingWindowCharsCount.decrement(cToRemove);
			slidingWindowCharsCount.increment(cToAdd);
			
			if(pCharsCount.contains(cToAdd) && slidingWindowCharsCount.equals(pCharsCount)) {
				occurences.add(i);
			}
		}
		return occurences;
	}
	
	private boolean validate(String s, String p) {
		if(s == null || s.trim().isEmpty()) {
			return false;
		}
		
		if(p == null || p.trim().isEmpty()) {
			return false;
		}
		
		if(s.length() < p.length()) {
			return false;
		}
		
		return true;
	}
	
	private static class CharCountMap {
		private static final byte LOWERCASE_A = 97;
		private static final byte LOWERCASE_Z = 122;
		
		private final int[] data;
		
		public CharCountMap(String s) {
			data = new int[LOWERCASE_Z - LOWERCASE_A + 1];
			for(char c:s.toCharArray()) {
				increment(c);
			}
		}
		
		public void increment(char c) {
			data[c - LOWERCASE_A] = data[c - LOWERCASE_A] + 1;
		}
		
		public void decrement(char c) {
			data[c - LOWERCASE_A] = data[c - LOWERCASE_A] - 1;
		}
		
		@Override
		public boolean equals(Object other) {
			return Arrays.equals(data, ((CharCountMap)other).data);
		}
		
		public boolean contains(char c) {
			return data[c - LOWERCASE_A] > 0;
		}
	}
}
