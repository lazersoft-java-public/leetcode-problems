package p1;

// https://leetcode.com/problems/queries-on-number-of-points-inside-a-circle/
public class Queries_on_number_of_points_inside_circle {

	public int[] countPoints(int[][] points, int[][] queries) {
		var queriesMatch = new int[queries.length];
		
		for(var i = 0; i < points.length; i++) {
			for(var j = 0; j < queries.length; j++) {
				var r = queries[j][2];
				
				var dx = Math.abs(points[i][0] - queries[j][0]);			
				
				if(dx > r) {
					continue;
				}
				
				var dy = Math.abs(points[i][1] - queries[j][1]);
				
				if(dy > r) {
					continue;
				}
				
				var distanceSquared = dx * dx + dy * dy;
				if(distanceSquared <= r * r) {
					queriesMatch[j] = queriesMatch[j] + 1;
				}
			}
		}
		
		
		return queriesMatch;
    }

}
