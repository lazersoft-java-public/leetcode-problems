package p1;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

public class House_robber_test {

	private House_robber solution = new House_robber();
	
	@Test
	public void test1() {
		var nums = new int[] {1, 2 ,3, 4};
		var solutionResult = solution.rob(nums);
		assertEquals(4, solutionResult);
	}
	
	@Test
	public void test2() {
		var nums = new int[] {2,7,9,3,1};
		var solutionResult = solution.rob(nums);
		assertEquals(12, solutionResult);
	}
}
