package p1;

import static org.junit.Assert.assertArrayEquals;

import org.junit.jupiter.api.Test;

public class Queries_on_number_of_points_inside_circle_test {
	
	private Queries_on_number_of_points_inside_circle solution = new Queries_on_number_of_points_inside_circle();
	
	@Test
	public void test1() {
		var points = new int[][] {{1,3},{3,3},{5,3},{2,2}};
		var queries = new int[][] {{2,3,1},{4,3,1},{1,1,2}};
		int[] solutionResult = solution.countPoints(points, queries);
		assertArrayEquals(new int[] {3, 2, 2}, solutionResult);
	}
	
	@Test
	public void test2() {
		var points = new int[][] {{1,1},{2,2},{3,3},{4,4},{5,5}};
		var queries = new int[][] {{1,2,2},{2,2,2},{4,3,2}, {4,3,3}};
		int[] solutionResult = solution.countPoints(points, queries);
		assertArrayEquals(new int[] {2, 3, 2, 4}, solutionResult);
	}
}
