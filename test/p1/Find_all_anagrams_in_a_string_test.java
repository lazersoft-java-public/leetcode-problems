package p1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

public class Find_all_anagrams_in_a_string_test {
	
	Find_all_anagrams_in_a_string solution = new Find_all_anagrams_in_a_string();

	@Test
	public void test1() {
		var s = "cbaebabacd";
		var p = "abc";
		List<Integer> solutionResult = solution.findAnagrams(s, p);
		assertEquals(Arrays.asList(0, 6), solutionResult);
	}
}
