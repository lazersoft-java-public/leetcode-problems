package p1;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

public class Letter_combinations_of_a_phone_number_test {

	private Letter_combinations_of_a_phone_number solution = new Letter_combinations_of_a_phone_number();
	
	@Test
	public void test1() {
		var digits = "23";
		var solutionResult = solution.letterCombinations(digits);
		assertEquals(sort(solutionResult), sort(Arrays.asList("ad","ae","af","bd","be","bf","cd","ce","cf")));
	}
	
	@Test
	public void test2() {
		var digits = "";
		var solutionResult = solution.letterCombinations(digits);
		assertEquals(solutionResult.size(), 0);
	}
	
	@Test
	public void test3() {
		var digits = "2";
		var solutionResult = solution.letterCombinations(digits);
		assertEquals(sort(solutionResult), sort(Arrays.asList("a","b","c")));
	}
	
	private List<String> sort(List<String> s) {
		Collections.sort(s);
		return s;
	}
}
